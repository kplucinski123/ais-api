package pl.plucinski.ais.token;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.plucinski.ais.Unsecured;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@RequestMapping("token")
public interface TokenRestResource {

    @Unsecured
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Return token", description = "Return active token for logged user", tags = {"token"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = TokenResponse.class)))
    })
    TokenResponse getToken(@RequestHeader(name = "token") String token, HttpServletRequest request);

    @Unsecured
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Create token", description = "Create new token for user", tags = {"token"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "successful operation", content = @Content(schema = @Schema(implementation = TokenResponse.class)))
    })
    @ResponseStatus(HttpStatus.CREATED)
    TokenResponse createToken(@Valid @RequestBody AuthenticateUserRequest request, HttpServletRequest servletRequest, HttpServletResponse response);

    @Unsecured
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Refresh token", description = "Refresh token", tags = {"token"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = TokenResponse.class)))
    })
    TokenResponse refreshToken(HttpServletRequest request);

    @Unsecured
    @DeleteMapping()
    @Operation(summary = "Delete token", description = "Deletes token for user", tags = {"token"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "successful operation", content = @Content(schema = @Schema(implementation = TokenResponse.class)))
    })
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void deleteToken(HttpServletRequest request);

}
