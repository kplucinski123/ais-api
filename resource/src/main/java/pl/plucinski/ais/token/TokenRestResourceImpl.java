package pl.plucinski.ais.token;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import pl.plucinski.ais.model.TokenUser;
import pl.plucinski.ais.refresh_token.RefreshTokenService;
import pl.plucinski.ais.service.AuthenticateService;
import pl.plucinski.ais.util.CookieUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
class TokenRestResourceImpl implements TokenRestResource {
    private final AuthenticateService authenticateService;
    private final RefreshTokenService refreshTokenService;
    private final CookieUtils cookieUtils;

    @Override
    public TokenResponse getToken(String token, HttpServletRequest request) {
        authenticateService.validateToken(token);
        return TokenResponse.builder()
                .token(token)
                .build();
    }

    @Override
    public TokenResponse createToken(AuthenticateUserRequest request, HttpServletRequest servletRequest, HttpServletResponse response) {
        TokenUser token = authenticateService.login(request);
        UUID refreshToken = refreshTokenService.createToken(token.getUserId(), servletRequest.getHeader("User-Agent"));
        cookieUtils.setCookie(response, "refresh-token", refreshToken.toString(), 30 * 24 * 60 * 60);
        return TokenResponse.builder()
                .userId(token.getUserId())
                .token(token.getToken())
                .build();
    }

    @Override
    public TokenResponse refreshToken(HttpServletRequest request) {
        UUID uuid = UUID.fromString(cookieUtils.getCookie(request, "refresh-token"));
        refreshTokenService.validateToken(uuid, request.getHeader("User-Agent"));
        TokenUser token = authenticateService.refreshToken(uuid);
        return TokenResponse.builder()
                .userId(token.getUserId())
                .token(token.getToken())
                .build();
    }

    @Override
    public void deleteToken(HttpServletRequest request) {
        UUID uuid = UUID.fromString(cookieUtils.getCookie(request, "refresh-token"));
        refreshTokenService.remove(uuid, request.getHeader("User-Agent"));
    }
}
