package pl.plucinski.ais.token;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.plucinski.ais.model.LoginUser;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@Builder
@RequiredArgsConstructor(onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)))
final class AuthenticateUserRequest implements LoginUser {
    @Email
    @NotBlank
    private final String email;

    @NotBlank
    private final String password;
}
