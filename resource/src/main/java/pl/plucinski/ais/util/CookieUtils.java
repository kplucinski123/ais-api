package pl.plucinski.ais.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface CookieUtils {
    /**
     * Method set cookie
     *
     * @param response {@link HttpServletResponse response} for set cookie
     * @param name {@link String cookie name}
     * @param value {@link String cookie value}
     * @param maxAge {@link Integer cookie max age}
     */
    void setCookie(HttpServletResponse response, String name, String value, Integer maxAge);

    /**
     * Method returns cookie
     *
     * @param request {@link HttpServletRequest request} foor return cookie
     * @param name {@link String cookie name}
     *
     * @return {@link String cookie value}
     */
    String getCookie(HttpServletRequest request, String name);
}
