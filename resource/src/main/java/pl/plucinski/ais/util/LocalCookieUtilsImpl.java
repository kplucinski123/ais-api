package pl.plucinski.ais.util;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Service
@Profile("local")
@RequiredArgsConstructor
class LocalCookieUtilsImpl implements CookieUtils {
    private final Map<String, String> cookies = new HashMap<>();

    @Override
    public void setCookie(HttpServletResponse response, String name, String value, Integer maxAge) {
        cookies.put(name, value);
    }

    @Override
    public String getCookie(HttpServletRequest request, String name) {
        return cookies.get(name);
    }
}
