package pl.plucinski.ais.user;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.plucinski.ais.model.RegisteredUser;
import pl.plucinski.ais.service.AuthenticateService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
class UserRestResourceImpl implements UserRestResource {
    private final AuthenticateService authenticateService;
    private final UserResourceMapper userResourceMapper;

    @Override
    public UserResponse register(@Valid @RequestBody CreateUserRequest request) {
        RegisteredUser registeredUser = authenticateService.register(request);
        return userResourceMapper.map(registeredUser);
    }
}
