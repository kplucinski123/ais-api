package pl.plucinski.ais.user;

import org.mapstruct.Mapper;
import pl.plucinski.ais.model.RegisteredUser;

@Mapper(componentModel = "spring")
public interface UserResourceMapper {
    UserResponse map(RegisteredUser registeredUser);
}
