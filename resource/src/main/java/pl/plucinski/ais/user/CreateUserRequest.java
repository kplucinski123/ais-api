package pl.plucinski.ais.user;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Length;
import pl.plucinski.ais.model.RegisterUser;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.util.UUID;

@Getter
@Builder
@RequiredArgsConstructor(onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)))
public final class CreateUserRequest implements RegisterUser {
    @NotBlank
    private final String email;

    @NotBlank
    @Length(min = 6)
    private final String userName;

    @NotBlank
    @Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$")
    private final String password;

    @NotBlank
    private final UUID avatarId;

    private final String key;
}
