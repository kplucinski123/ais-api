package pl.plucinski.ais.handler;

import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import pl.plucinski.ais.BusinessException;
import pl.plucinski.ais.BusinessExceptionCode;

@Log4j2
@RestControllerAdvice
public class BusinessAdvice {
    @ExceptionHandler
    public ResponseEntity<CommonError> handle(InternalAuthenticationServiceException exception) {
        log.info("Wrong authentication", exception);
        return create(HttpStatus.BAD_REQUEST, "WRONG_AUTHENTICATION", exception.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity<CommonError> handle(BusinessException exception) {
        log.info("Business exception", exception);
        BusinessExceptionCode exceptionCode = exception.getCode();
        return create(HttpStatus.resolve(exceptionCode.getResponseCode()), exceptionCode.name(), exception.getMessage());
    }

    @ExceptionHandler
    public ResponseEntity<CommonError> handle(AccessDeniedException exception) {
        log.warn("Access denied", exception);
        return create(HttpStatus.FORBIDDEN, StringUtils.EMPTY, "Access denied");
    }

    @ExceptionHandler
    public ResponseEntity<CommonError> handle(Exception exception) {
        log.error("General error", exception);
        return create(HttpStatus.BAD_REQUEST, StringUtils.EMPTY, "General error");
    }

    private ResponseEntity<CommonError> create(HttpStatus status, String code, String message) {
        return ResponseEntity
                .status(status)
                .body(CommonError.builder()
                        .message(message)
                        .code(code)
                        .build());
    }
}
