package pl.plucinski.ais.notification.vessel;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RestController;
import pl.plucinski.ais.model.LoggedUser;
import pl.plucinski.ais.notification.VesselNotificationTask;
import pl.plucinski.ais.notification.VesselNotificationTaskService;

@RestController
@RequiredArgsConstructor
class VesselNotificationRestResourceImpl implements VesselNotificationRestResource {
    private final VesselNotificationTaskService vesselNotificationTaskService;
    private final VesselNotificationResourceMapper vesselNotificationResourceMapper;

    @Override
    @Secured("ROLE_USER")
    public void createTask(LoggedUser user, Long vesselId, VesselNotificationRequest request) {
        VesselNotificationTask task = vesselNotificationResourceMapper.map(request);
        vesselNotificationTaskService.create(user.getId(), vesselId, task);
    }
}
