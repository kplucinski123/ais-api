package pl.plucinski.ais.notification.vessel;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@Builder
@RequiredArgsConstructor(onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)))
public class VesselNotificationRequest {
    private final Double maxLatitude;
    private final Double minLatitude;
    private final Double maxLongitude;
    private final Double minLongitude;
    private final Integer deadLineInMinutes;
}
