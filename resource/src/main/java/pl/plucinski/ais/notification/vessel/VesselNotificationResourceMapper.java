package pl.plucinski.ais.notification.vessel;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.plucinski.ais.notification.VesselNotificationTask;

import java.time.LocalDateTime;

@Mapper(componentModel = "spring", imports = LocalDateTime.class)
interface VesselNotificationResourceMapper {
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "user", ignore = true)
    @Mapping(target = "vessel", ignore = true)
    @Mapping(target = "deadLine", source = "deadLineInMinutes")
    @Mapping(target = "nextCheck", expression = "java(LocalDateTime.now())")
    VesselNotificationTask map(VesselNotificationRequest request);

    default LocalDateTime deadLine(Integer minutes) {
        return LocalDateTime.now().plusMinutes(minutes);
    }
}
