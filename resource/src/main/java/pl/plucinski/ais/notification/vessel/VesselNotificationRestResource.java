package pl.plucinski.ais.notification.vessel;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.plucinski.ais.Logged;
import pl.plucinski.ais.model.LoggedUser;

@RequestMapping("notifications/vessels/{vesselId}")
@Tag(name = "vesselNotification", description = "Vessel notification API")
public interface VesselNotificationRestResource {

    @PostMapping(value = "tasks", consumes = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Add task for vessel notification", description = "Add task for vessel notification", tags = {"vesselNotification"})
    @ApiResponses(value = @ApiResponse(responseCode = "204", description = "successful operation"))
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void createTask(@Logged LoggedUser user, @PathVariable("vesselId") Long vesselId, @RequestBody VesselNotificationRequest request);
}
