package pl.plucinski.ais.vessel;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@Builder
@RequiredArgsConstructor(onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES)))
public class VesselResponse {
    private final Long id;
    private final String mmsi;
    private final Integer shipType;
    private final String country;
    private final String name;
    private final boolean favorite;
}
