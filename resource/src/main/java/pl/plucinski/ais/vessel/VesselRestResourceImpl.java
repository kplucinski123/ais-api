package pl.plucinski.ais.vessel;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RestController;
import pl.plucinski.ais.model.LoggedUser;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class VesselRestResourceImpl implements VesselRestResource {
    private final VesselResourceMapper vesselResourceMapper;
    private final VesselService vesselService;

    @Override
    @Secured("ROLE_USER")
    public VesselsResponse getAll(LoggedUser user) {
        List<VesselResponse> vessels = vesselService.getAll(user.getId()).stream()
                .map(vesselResourceMapper::map)
                .collect(Collectors.toList());
        return VesselsResponse.builder()
                .vessels(vessels)
                .build();
    }

    @Override
    @Secured("ROLE_USER")
    public VesselDetailsResponse getDetails(Long vesselId) {
        VesselDetails details = vesselService.getDetails(vesselId);
        return vesselResourceMapper.map(details);
    }

    @Override
    @Secured("ROLE_USER")
    public void addToFavorite(LoggedUser user, Long vesselId) {
        vesselService.addToFavorite(user.getId(), vesselId);
    }

    @Override
    @Secured("ROLE_USER")
    public void removeFromFavorite(LoggedUser user, Long vesselId) {
        vesselService.removeFromFavorite(user.getId(), vesselId);
    }

    @Override
    @Secured("ROLE_USER")
    public VesselCoordinatesResponse getCoordinates(Long vesselId) {
        VesselCoordinates coordinates = vesselService.getCoordinates(vesselId);
        return vesselResourceMapper.map(coordinates);
    }
}
