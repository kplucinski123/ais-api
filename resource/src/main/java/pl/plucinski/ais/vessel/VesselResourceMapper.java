package pl.plucinski.ais.vessel;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VesselResourceMapper {
    VesselResponse map(Vessel model);
    VesselDetailsResponse map(VesselDetails model);
    VesselCoordinatesResponse map(VesselCoordinates model);
}
