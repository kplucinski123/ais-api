package pl.plucinski.ais.vessel;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.plucinski.ais.Logged;
import pl.plucinski.ais.model.LoggedUser;

@RequestMapping("vessels")
@Tag(name = "vessels", description = "API for Vessels")
public interface VesselRestResource {

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Return vessels", description = "Return all vessels", tags = {"vessel"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = VesselsResponse.class)))
    })
    VesselsResponse getAll(@Logged LoggedUser user);

    @GetMapping(value = "{vesselId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Return vessel details", description = "Return vessel details", tags = {"vessel"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = VesselDetailsResponse.class)))
    })
    VesselDetailsResponse getDetails(@PathVariable("vesselId") Long vesselId);

    @PostMapping(value = "{vesselId}/favorite")
    @Operation(summary = "Add vessel to favorite", description = "Add vessel to favorite", tags = {"vessel"})
    @ApiResponses(value = @ApiResponse(responseCode = "204", description = "successful operation"))
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void addToFavorite(@Logged LoggedUser user, @PathVariable("vesselId") Long vesselId);

    @DeleteMapping(value = "{vesselId}/favorite")
    @Operation(summary = "Remove vessel from favorite", description = "Remove vessel from favorite", tags = {"vessel"})
    @ApiResponses(value = @ApiResponse(responseCode = "204", description = "successful operation"))
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void removeFromFavorite(@Logged LoggedUser user, @PathVariable("vesselId") Long vesselId);

    @GetMapping(value = "{vesselId}/coordinates", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(summary = "Return vessel coordinates", description = "Return vessel coordinates", tags = {"vessel"})
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful operation", content = @Content(schema = @Schema(implementation = VesselCoordinatesResponse.class)))
    })
    VesselCoordinatesResponse getCoordinates(@PathVariable("vesselId") Long vesselId);
}
