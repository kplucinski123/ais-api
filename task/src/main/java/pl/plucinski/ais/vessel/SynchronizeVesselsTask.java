package pl.plucinski.ais.vessel;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
@Profile("scheduler")
@RequiredArgsConstructor
public class SynchronizeVesselsTask {
    private final VesselRepository vesselRepository;

    @PostConstruct
    public void importVessels() {
        vesselRepository.synchronize();
    }

    @Scheduled(cron = "${ais.task.synchronize-vessels-cron}")
    @SchedulerLock(name = "SynchronizeVessels", lockAtLeastFor = "PT1M", lockAtMostFor = "PT5M")
    public void synchronize() {
        vesselRepository.synchronize();
    }
}
