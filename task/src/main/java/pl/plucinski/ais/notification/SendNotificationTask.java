package pl.plucinski.ais.notification;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Profile("scheduler")
@RequiredArgsConstructor
public class SendNotificationTask {
    private final NotificationRepository notificationRepository;

    @Scheduled(cron = "${ais.task.send-notification-cron}")
    public void sendNotifications() {
        notificationRepository.findForProcessing().forEach(this::sendNotification);
    }

    private void sendNotification(Notification notification) {
        try {
            notificationRepository.sendNotification(notification);
            notification = notification.toBuilder()
                    .status(NotificationStatus.SENT)
                    .build();
        } catch (Exception exception) {
            notification = notification.toBuilder()
                    .status(NotificationStatus.ERROR)
                    .build();
        }
        notificationRepository.save(notification);
    }
}
