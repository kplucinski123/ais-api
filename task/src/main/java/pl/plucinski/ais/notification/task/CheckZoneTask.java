package pl.plucinski.ais.notification.task;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import pl.plucinski.ais.notification.Notification;
import pl.plucinski.ais.notification.NotificationRepository;
import pl.plucinski.ais.notification.NotificationStatus;
import pl.plucinski.ais.notification.NotificationTaskStatus;
import pl.plucinski.ais.notification.VesselNotificationTask;
import pl.plucinski.ais.vessel.Vessel;
import pl.plucinski.ais.vessel.VesselCoordinates;
import pl.plucinski.ais.vessel.VesselRepository;

import java.time.LocalDateTime;

import static pl.plucinski.ais.notification.NotificationTaskStatus.NEW;
import static pl.plucinski.ais.notification.NotificationTaskStatus.EXPIRED;
import static pl.plucinski.ais.notification.NotificationTaskStatus.FINISHED;

@Component
@Profile("scheduler")
@RequiredArgsConstructor
public class CheckZoneTask {
    private final VesselNotificationTaskRepository vesselNotificationTaskRepository;
    private final NotificationRepository notificationRepository;
    private final VesselRepository vesselRepository;

    @Scheduled(cron = "${ais.task.check-vessels-zone-cron}")
    public void checkZone() {
        vesselNotificationTaskRepository.findForZoneChecking().forEach(this::checkZone);
    }

    private void checkZone(VesselNotificationTask task) {
        Vessel vessel = task.getVessel();
        VesselCoordinates coordinates = vesselRepository.getCoordinates(vessel);
        if (checkVesselIsInZone(coordinates, task)) {
            Notification notification = Notification.builder()
                    .task(task)
                    .status(NotificationStatus.NEW)
                    .build();
            notificationRepository.save(notification);
            task = task.toBuilder()
                    .status(FINISHED)
                    .build();
        } else {
            LocalDateTime nextCheck = LocalDateTime.now().plusMinutes(5);
            NotificationTaskStatus status = nextCheck.isBefore(task.getDeadLine()) ? NEW : EXPIRED;
            task = task.toBuilder()
                    .nextCheck(nextCheck)
                    .status(status)
                    .build();
        }
        vesselNotificationTaskRepository.save(task);
    }

    private boolean checkVesselIsInZone(VesselCoordinates coordinates, VesselNotificationTask task) {
        Double maxLatitude = task.getMaxLatitude();
        Double minLatitude = task.getMinLatitude();
        Double maxLongitude = task.getMaxLongitude();
        Double minLongitude = task.getMinLongitude();
        return coordinates.getLongitude() >= minLongitude &&
                coordinates.getLongitude() <= maxLongitude &&
                coordinates.getLatitude() >= minLatitude &&
                coordinates.getLatitude() <= maxLatitude;
    }
}
