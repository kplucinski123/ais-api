package pl.plucinski.ais.refresh_token;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.plucinski.ais.BusinessException;
import pl.plucinski.ais.BusinessExceptionCode;
import pl.plucinski.ais.token.RefreshToken;
import pl.plucinski.ais.token.RefreshTokenRepository;
import pl.plucinski.ais.token.RefreshTokenStatus;
import pl.plucinski.ais.user.UserService;
import pl.plucinski.ais.user.User;

import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
class RefreshTokenServiceImpl implements RefreshTokenService {
    private final UserService userService;
    private final RefreshTokenRepository refreshTokenRepository;

    @Override
    public UUID createToken(Long userId, String userAgent) {
        User user = userService.findById(userId);
        RefreshToken refreshToken = RefreshToken.builder()
                .status(RefreshTokenStatus.ACTIVE)
                .expirationTime(LocalDateTime.now().plusDays(30L))
                .user(user)
                .userAgent(userAgent)
                .id(UUID.randomUUID())
                .build();
        refreshTokenRepository.saveToken(refreshToken);
        return refreshToken.getId();
    }

    @Override
    public void validateToken(UUID uuid, String userAgent) {
        refreshTokenRepository.getToken(uuid)
                .filter(token -> token.getStatus() == RefreshTokenStatus.ACTIVE)
                .filter(this::validateExpiationTime)
                .map(RefreshToken::getUserAgent)
                .filter(userAgent::equals)
                .orElseThrow(() -> new BusinessException(BusinessExceptionCode.INVALID_REFRESH_TOKEN));
    }

    @Override
    public Optional<RefreshToken> get(UUID uuid) {
        return refreshTokenRepository.getToken(uuid);
    }

    @Override
    public void remove(UUID uuid, String userAgent) {
        validateToken(uuid, userAgent);
        refreshTokenRepository.changeTokenStatus(uuid, RefreshTokenStatus.LOGOUT);
    }

    private boolean validateExpiationTime(RefreshToken refreshToken) {
        return LocalDateTime.now().isBefore(refreshToken.getExpirationTime());
    }
}
