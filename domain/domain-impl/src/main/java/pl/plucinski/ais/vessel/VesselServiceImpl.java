package pl.plucinski.ais.vessel;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.plucinski.ais.BusinessException;
import pl.plucinski.ais.BusinessExceptionCode;
import pl.plucinski.ais.user.UserService;
import pl.plucinski.ais.vessel.favorite.VesselFavorite;
import pl.plucinski.ais.vessel.favorite.VesselFavoriteRepository;
import pl.plucinski.ais.vessel.favorite.VesselFavoriteStatus;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
class VesselServiceImpl implements VesselService {
    private final UserService userService;
    private final VesselRepository vesselRepository;
    private final VesselFavoriteRepository vesselFavoriteRepository;

    @Override
    public List<Vessel> getAll(Long userId) {
        List<Long> favoriteVesselsId = vesselFavoriteRepository.findAll(userId).stream()
                .map(VesselFavorite::getVessel)
                .map(Vessel::getId)
                .collect(Collectors.toList());
        return vesselRepository.getAll().stream()
                .map(vessel -> mapFavorite(vessel, favoriteVesselsId))
                .collect(Collectors.toList());
    }

    @Override
    public VesselDetails getDetails(Long vesselId) {
        Vessel vessel = findById(vesselId);
        return vesselRepository.getDetails(vessel);
    }

    @Override
    public void addToFavorite(Long userId, Long vesselId) {
        VesselFavorite vesselFavorite = vesselFavoriteRepository.find(userId, vesselId)
                .map(VesselFavorite::toBuilder)
                .map(builder -> builder.status(VesselFavoriteStatus.ACTIVE))
                .map(VesselFavorite.Builder::build)
                .orElseGet(() -> createVesselFavorite(userId, vesselId));
        vesselFavoriteRepository.save(vesselFavorite);
    }

    @Override
    public void removeFromFavorite(Long userId, Long vesselId) {
        vesselFavoriteRepository.find(userId, vesselId)
                .map(VesselFavorite::toBuilder)
                .map(builder -> builder.status(VesselFavoriteStatus.REMOVED))
                .map(VesselFavorite.Builder::build)
                .ifPresent(vesselFavoriteRepository::save);
    }

    @Override
    public VesselCoordinates getCoordinates(Long vesselId) {
        Vessel vessel = findById(vesselId);
        return vesselRepository.getCoordinates(vessel);
    }

    @Override
    public Vessel findById(Long vesselId) {
        return vesselRepository.findById(vesselId)
                .orElseThrow(() -> new BusinessException(BusinessExceptionCode.INVALID_VESSEL_ID));
    }

    private Vessel mapFavorite(Vessel vessel, List<Long> favoriteVesselsId) {
        return vessel.toBuilder()
                .favorite(favoriteVesselsId.contains(vessel.getId()))
                .build();
    }

    private VesselFavorite createVesselFavorite(Long userId, Long vesselId) {
        Vessel vessel = findById(vesselId);
        return VesselFavorite.builder()
                .user(userService.findById(userId))
                .vessel(vessel)
                .status(VesselFavoriteStatus.ACTIVE)
                .build();
    }
}
