package pl.plucinski.ais.notification.task;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.plucinski.ais.notification.NotificationTaskStatus;
import pl.plucinski.ais.notification.VesselNotificationTask;
import pl.plucinski.ais.notification.VesselNotificationTaskService;
import pl.plucinski.ais.user.User;
import pl.plucinski.ais.user.UserService;
import pl.plucinski.ais.vessel.Vessel;
import pl.plucinski.ais.vessel.VesselService;

@Service
@RequiredArgsConstructor
class VesselNotificationTaskServiceImpl implements VesselNotificationTaskService {
    private final VesselService vesselService;
    private final UserService userService;
    private final VesselNotificationTaskRepository vesselNotificationTaskRepository;

    @Override
    public void create(Long userId, Long vesselId, VesselNotificationTask task) {
        Vessel vessel = vesselService.findById(vesselId);
        User user = userService.findById(userId);
        task = task.toBuilder()
                .status(NotificationTaskStatus.NEW)
                .vessel(vessel)
                .user(user)
                .build();
        vesselNotificationTaskRepository.save(task);
    }
}
