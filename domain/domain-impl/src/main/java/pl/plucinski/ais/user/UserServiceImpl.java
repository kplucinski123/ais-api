package pl.plucinski.ais.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.plucinski.ais.BusinessException;
import pl.plucinski.ais.BusinessExceptionCode;

@Service
@RequiredArgsConstructor
class UserServiceImpl implements UserService {
    private final UserRepository userRepository;

    @Override
    public User findByEmail(String email) {
        return userRepository.findUserByEmail(email)
                .orElseThrow(() -> new BusinessException(BusinessExceptionCode.USER_NOT_FOUND));
    }

    @Override
    public User findById(Long id) {
        return userRepository.findUserById(id)
                .orElseThrow(() -> new BusinessException(BusinessExceptionCode.USER_NOT_FOUND));
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public boolean checkUserExists(String email) {
        return userRepository.findUserByEmail(email).isPresent();
    }
}
