package pl.plucinski.ais.vessel;

import pl.plucinski.ais.BusinessException;

import java.util.List;

public interface VesselService {

    /**
     * Method returns all vessels
     *
     * @param userId {@link Long} user identifier
     *
     * @return {@link List list} of {@link Vessel vessels}
     */
    List<Vessel> getAll(Long userId);

    /**
     * Method returns vessel details
     *
     * @param vesselId {@link Long} vessel identifier
     *
     * @return {@link VesselDetails details}
     */
    VesselDetails getDetails(Long vesselId);

    /**
     * Method adds vessel to favorite
     *
     * @param userId {@link Long} user identifier
     * @param vesselId {@link Long} vessel identifier
     */
    void addToFavorite(Long userId, Long vesselId);

    /**
     * Method removes vessel from favorite
     *
     * @param userId {@link Long} user identifier
     * @param vesselId {@link Long} vessel identifier
     */
    void removeFromFavorite(Long userId, Long vesselId);

    /**
     * Method returns vessel coordinates
     *
     * @param vesselId {@link Long} vessel identifier
     */
    VesselCoordinates getCoordinates(Long vesselId);

    /**
     * Method returns vessel by identifier
     *
     * @param vesselId {@link Long} vessel identifier
     *
     * @throws BusinessException when vessel not found
     *
     * @return {@link Vessel found vessel}
     */
    Vessel findById(Long vesselId);
}
