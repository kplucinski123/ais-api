package pl.plucinski.ais.refresh_token;

import pl.plucinski.ais.token.RefreshToken;

import java.util.Optional;
import java.util.UUID;

public interface RefreshTokenService {
    /**
     * Method creates refresh token
     *
     * @param userId {@link Long} user identifier
     * @param userAgent {@link String} user agent
     *
     * @return {@link UUID refresh token}
     */
    UUID createToken(Long userId, String userAgent);

    /**
     * Method validates refresh token
     *
     * @param uuid {@link UUID} refresh token
     * @param userAgent {@link String} user agent
     */
    void validateToken(UUID uuid, String userAgent);

    /**
     * Method returns optional of refresh token
     *
     * @param uuid {@link UUID} refresh token uuid
     *
     * @return {@link Optional optional} of {@link RefreshToken refresh token}
     */
    Optional<RefreshToken> get(UUID uuid);

    /**
     * Method removes refresh token
     *
     * @param uuid {@link UUID} refresh token
     * @param userAgent {@link String} user agent
     */
    void remove(UUID uuid, String userAgent);
}
