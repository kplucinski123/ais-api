package pl.plucinski.ais.notification;

public interface VesselNotificationTaskService {
    /**
     * Method creates new vessel notification task
     *
     * @param userId {@link Long} user identifier
     * @param vesselId {@link Long} vessel identifier
     * @param task {@link VesselNotificationTask task for creation}
     */
    void create(Long userId, Long vesselId, VesselNotificationTask task);
}
