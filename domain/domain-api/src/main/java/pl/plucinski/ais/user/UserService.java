package pl.plucinski.ais.user;

public interface UserService {

    /**
     * Method returns user by email
     *
     * @param email {@link String} user email
     *
     * @return found {@link User user}
     */
    User findByEmail(String email);

    /**
     * Method returns user by identifier
     *
     * @param id {@link String} user identifier
     *
     * @return found {@link User user}
     */
    User findById(Long id);

    /**
     * Method checks if user exist
     *
     * @param email {@link String} user email
     *
     * @return result if user exists
     */
    boolean checkUserExists(String email);

    /**
     * Method saves user
     *
     * @param user {@link User} for save
     *
     * @return saved {@link User user}
     */
    User save(User user);
}
