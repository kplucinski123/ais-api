package pl.plucinski.ais.token;

import lombok.Builder;
import lombok.Getter;
import pl.plucinski.ais.user.User;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Builder(toBuilder = true)
public final class RefreshToken {
    private final UUID id;
    private final LocalDateTime creationDate;
    private final RefreshTokenStatus status;
    private final LocalDateTime expirationTime;
    private final String userAgent;
    private final User user;
}
