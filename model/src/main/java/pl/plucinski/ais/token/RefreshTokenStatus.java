package pl.plucinski.ais.token;

public enum RefreshTokenStatus {
    ACTIVE, LOGOUT
}
