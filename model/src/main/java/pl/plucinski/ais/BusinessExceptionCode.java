package pl.plucinski.ais;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public enum BusinessExceptionCode {
    USER_NOT_FOUND(400, "User not found"),
    USER_EXISTS(400, "User exists"),
    INVALID_TOKEN(403, "Token is invalid"),
    INVALID_REFRESH_TOKEN(403, "Refresh token is invalidate"),
    INVALID_VESSEL_ID(400, "Invalid vessel identifier");

    private final int responseCode;
    private final String message;
}
