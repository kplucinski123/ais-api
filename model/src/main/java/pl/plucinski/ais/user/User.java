package pl.plucinski.ais.user;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder(toBuilder = true, builderClassName = "Builder")
public final class User {
    private final Long id;
    private final LocalDateTime creationDate;
    private final String role;
    private final String userName;
    private final String email;
    private final String password;
}
