package pl.plucinski.ais.vessel;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class VesselDetails {
    private final String mmsi;
    private final String name;
    private final String imo;
    private final String ircs;
    private final String country;
    private final String shipType;
    private final String cog;
    private final String shipRegisterName;
    private final String aisName;
}
