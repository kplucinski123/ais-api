package pl.plucinski.ais.vessel.favorite;

public enum VesselFavoriteStatus {
    ACTIVE, REMOVED
}
