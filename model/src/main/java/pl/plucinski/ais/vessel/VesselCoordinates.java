package pl.plucinski.ais.vessel;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class VesselCoordinates {
    private final Double longitude;
    private final Double latitude;
}
