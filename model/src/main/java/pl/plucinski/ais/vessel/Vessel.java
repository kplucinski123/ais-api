package pl.plucinski.ais.vessel;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder(toBuilder = true, builderClassName = "Builder")
public class Vessel {
    private final Long id;
    private final LocalDateTime creationDate;
    private final String mmsi;
    private final Integer shipType;
    private final String country;
    private final String name;
    private final boolean favorite;
}
