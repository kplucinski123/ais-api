package pl.plucinski.ais.vessel.favorite;

import lombok.Builder;
import lombok.Getter;
import pl.plucinski.ais.user.User;
import pl.plucinski.ais.vessel.Vessel;

import java.time.LocalDateTime;

@Getter
@Builder(toBuilder = true, builderClassName = "Builder")
public class VesselFavorite {
    private final Long id;
    private final LocalDateTime creationDate;
    private final VesselFavoriteStatus status;
    private final User user;
    private final Vessel vessel;
}
