package pl.plucinski.ais.notification;

import lombok.Getter;

@Getter
public enum NotificationStatus {
    NEW, PROCESSING, SENT, ERROR
}
