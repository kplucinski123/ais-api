package pl.plucinski.ais.notification;

import lombok.Builder;
import lombok.Getter;
import pl.plucinski.ais.user.User;
import pl.plucinski.ais.vessel.Vessel;

import java.time.LocalDateTime;

@Getter
@Builder(toBuilder = true, builderClassName = "Builder")
public class VesselNotificationTask {
    private final Long id;
    private final LocalDateTime creationDate;
    private final Double maxLatitude;
    private final Double minLatitude;
    private final Double maxLongitude;
    private final Double minLongitude;
    private final NotificationTaskStatus status;
    private final LocalDateTime deadLine;
    private final LocalDateTime nextCheck;
    private final User user;
    private final Vessel vessel;
}
