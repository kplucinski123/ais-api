package pl.plucinski.ais.notification;

import lombok.Getter;

@Getter
public enum NotificationTaskStatus {
    NEW, PROCESSING, FINISHED, EXPIRED
}
