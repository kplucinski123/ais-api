package pl.plucinski.ais.notification;

import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder(toBuilder = true, builderClassName = "Builder")
public class Notification {
    private final Long id;
    private final LocalDateTime creationDate;
    private final NotificationStatus status;
    private final VesselNotificationTask task;
}
