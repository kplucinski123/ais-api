package pl.plucinski.ais.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@RequiredArgsConstructor
class UserRepositoryImpl implements UserRepository {
    private final UserJpaRepository userJpaRepository;
    private final UserEntityMapper userEntityMapper;

    @Override
    public User save(User user) {
        UserEntity userEntity = userEntityMapper.mapWithDependencies(user);
        UserEntity saved = userJpaRepository.save(userEntity);
        return userEntityMapper.map(saved);
    }

    @Override
    public Optional<User> findUserByEmail(String email) {
        return userJpaRepository.findByEmail(email)
                .map(userEntityMapper::map);
    }

    @Override
    public Optional<User> findUserById(Long id) {
        return userJpaRepository.findById(id)
                .map(userEntityMapper::map);
    }
}
