package pl.plucinski.ais.external;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(
        onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES))
)
public class DetailsDto {
    private final String mmsi;
    private final String name;
    private final String imo;
    private final String ircs;
    private final String country;
    private final String shipType;
    private final String cog;
    private final String shipRegisterName;
    private final String aisName;
}
