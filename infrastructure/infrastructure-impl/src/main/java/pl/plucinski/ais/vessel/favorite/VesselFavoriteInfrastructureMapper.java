package pl.plucinski.ais.vessel.favorite;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
interface VesselFavoriteInfrastructureMapper {
    @Mapping(target = "modificationDate", ignore = true)
    VesselFavoriteEntity map(VesselFavorite model);

    VesselFavorite map(VesselFavoriteEntity entity);
}
