package pl.plucinski.ais.notification.task;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.plucinski.ais.notification.VesselNotificationTask;

@Mapper(componentModel = "spring")
interface VesselNotificationTaskInfrastructureMapper {
    @Mapping(target = "modificationDate", ignore = true)
    VesselNotificationTaskEntity map(VesselNotificationTask model);

    VesselNotificationTask map(VesselNotificationTaskEntity entity);
}
