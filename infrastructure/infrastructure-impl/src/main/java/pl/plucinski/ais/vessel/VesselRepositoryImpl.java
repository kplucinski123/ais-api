package pl.plucinski.ais.vessel;

import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.plucinski.ais.external.AisClient;
import pl.plucinski.ais.external.CoordinatesDto;
import pl.plucinski.ais.external.DetailsDto;
import pl.plucinski.ais.external.VesselClient;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
class VesselRepositoryImpl implements VesselRepository {
    private final static Integer MAX_BATCH_SIZE = 1000;

    private final AisClient aisClient;
    private final VesselClient vesselClient;
    private final VesselInfrastructureMapper vesselInfrastructureMapper;
    private final VesselJpaRepository vesselJpaRepository;

    @Override
    public List<Vessel> getAll() {
        return vesselJpaRepository.findAll().stream()
                .map(vesselInfrastructureMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Vessel> findById(Long id) {
        return vesselJpaRepository.findById(id)
                .map(vesselInfrastructureMapper::map);
    }

    @Override
    public VesselDetails getDetails(Vessel vessel) {
        DetailsDto details = vesselClient.getDetails(vessel.getMmsi());
        return vesselInfrastructureMapper.map(details);
    }

    @Override
    public VesselCoordinates getCoordinates(Vessel vessel) {
        CoordinatesDto coordinates = aisClient.getCoordinates(vessel.getMmsi());
        return vesselInfrastructureMapper.map(coordinates);
    }

    @Override
    public void synchronize() {
        List<VesselEntity> newVessels = aisClient.getAllPositions().stream()
                .map(vesselInfrastructureMapper::map)
                .collect(Collectors.toList());
        List<VesselEntity> oldVessels = vesselJpaRepository.findAll();

        List<String> oldVesselsMmsi = oldVessels.stream()
                .map(VesselEntity::getMmsi)
                .collect(Collectors.toList());

        List<String> newVesselsMmsi = oldVessels.stream()
                .map(VesselEntity::getMmsi)
                .collect(Collectors.toList());

        List<VesselEntity> forRemoving = oldVessels.stream()
                .filter(vessel -> !newVesselsMmsi.contains(vessel.getMmsi()))
                .collect(Collectors.toList());
        removeAll(forRemoving);

        List<VesselEntity> forSaving = newVessels.stream()
                .filter(vessel -> !oldVesselsMmsi.contains(vessel.getMmsi()))
                .collect(Collectors.toList());
        saveAll(forSaving);
    }

    private void removeAll(List<VesselEntity> vessels) {
        Lists.partition(vessels, MAX_BATCH_SIZE)
                .forEach(vesselJpaRepository::deleteAll);
    }

    public void saveAll(List<VesselEntity> vessels) {
        Lists.partition(vessels, MAX_BATCH_SIZE)
                .forEach(vesselJpaRepository::saveAll);
    }
}
