package pl.plucinski.ais.external.token;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "refreshToken", url = "#{refreshTokenProperties.url}")
public interface RefreshTokenClient {

    @PostMapping(consumes = "application/x-www-form-urlencoded")
    RefreshTokenDto generateToken(@RequestBody String requestBody);
}
