package pl.plucinski.ais.external.token;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties("ais.externaltoken.refresh")
public class RefreshTokenProperties {
    private static final String BODY = "client_id=%s&scope=api&client_secret=%s&grant_type=client_credentials";

    private String url;
    private String client;
    private String secret;

    public String getBody() {
        return String.format(BODY, client, secret);
    }
}
