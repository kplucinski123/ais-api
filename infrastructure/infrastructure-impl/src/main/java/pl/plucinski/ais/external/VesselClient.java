package pl.plucinski.ais.external;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "vessel", url = "${ais.vessel.url}")
public interface VesselClient {
    @GetMapping("open/{mmsi}")
    DetailsDto getDetails(@PathVariable("mmsi") String mmsi);
}
