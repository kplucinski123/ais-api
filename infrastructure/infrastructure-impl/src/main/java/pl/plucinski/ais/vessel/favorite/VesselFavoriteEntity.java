package pl.plucinski.ais.vessel.favorite;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.PersistenceConstructor;
import pl.plucinski.ais.user.UserEntity;
import pl.plucinski.ais.vessel.VesselEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDateTime;

@Getter
@Entity
@Table(name = "vessel_favorites", uniqueConstraints = @UniqueConstraint(columnNames={"user_id", "vessel_id"}))
@Builder(toBuilder = true, builderClassName = "Builder")
@RequiredArgsConstructor(onConstructor = @__(@PersistenceConstructor))
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class VesselFavoriteEntity {
    private static final String sequence = "vessel_favorites_id_seq";

    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = sequence, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = sequence, sequenceName = sequence)
    private final Long id;

    @CreationTimestamp
    @Column(nullable = false)
    private final LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(nullable = false)
    private final LocalDateTime modificationDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private final VesselFavoriteStatus status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private final UserEntity user;

    @ManyToOne
    @JoinColumn(name = "vessel_id")
    private final VesselEntity vessel;
}
