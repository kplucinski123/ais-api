package pl.plucinski.ais.notification;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import java.util.List;

interface NotificationJpaRepository extends JpaRepository<NotificationEntity, Long> {
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @Query("from NotificationEntity entity where entity.status = 'NEW'")
    List<NotificationEntity> findForProcessing();

    @Modifying
    @Query("update NotificationEntity entity set entity.status = 'PROCESSING' where entity.id in :ids")
    void setProcessingStatus(@Param("ids") List<Long> ids);
}
