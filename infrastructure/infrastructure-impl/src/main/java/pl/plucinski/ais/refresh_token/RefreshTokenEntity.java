package pl.plucinski.ais.refresh_token;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.PersistenceConstructor;
import pl.plucinski.ais.token.RefreshTokenStatus;
import pl.plucinski.ais.user.UserEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Entity
@Table(name = "refresh_tokens")
@Builder(toBuilder = true, builderClassName = "Builder")
@RequiredArgsConstructor(onConstructor = @__(@PersistenceConstructor))
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public final class RefreshTokenEntity {
    @Id
    private final UUID id;

    @CreationTimestamp
    @Column(nullable = false)
    private final LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(nullable = false)
    private final LocalDateTime modificationDate;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private final RefreshTokenStatus status;

    @Column(nullable = false)
    private final LocalDateTime expirationTime;

    @Column(nullable = false)
    private final String userAgent;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private final UserEntity user;
}
