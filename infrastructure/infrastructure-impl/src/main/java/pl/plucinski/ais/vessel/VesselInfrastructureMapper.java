package pl.plucinski.ais.vessel;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.plucinski.ais.external.CoordinatesDto;
import pl.plucinski.ais.external.DetailsDto;
import pl.plucinski.ais.external.PositionDto;
import pl.plucinski.ais.external.token.GeometryDto;

@Mapper(componentModel = "spring")
interface VesselInfrastructureMapper {
    @Mapping(target = "creationDate", ignore = true)
    @Mapping(target = "modificationDate", ignore = true)
    @Mapping(target = "id", ignore = true)
    VesselEntity map(PositionDto dto);

    @Mapping(target = "favorite", ignore = true)
    Vessel map(VesselEntity entity);

    VesselDetails map(DetailsDto dto);

    default VesselCoordinates map(CoordinatesDto dto) {
        GeometryDto geometry = dto.getGeometry();
        return VesselCoordinates.builder()
                .longitude(geometry.getCoordinates()[0])
                .latitude(geometry.getCoordinates()[1])
                .build();
    }
}
