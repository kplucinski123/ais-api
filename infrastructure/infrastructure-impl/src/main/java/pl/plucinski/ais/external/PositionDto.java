package pl.plucinski.ais.external;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(
        onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES))
)
public class PositionDto {
    private final String mmsi;
    private final Integer shipType;
    private final String country;
    private final String name;
}
