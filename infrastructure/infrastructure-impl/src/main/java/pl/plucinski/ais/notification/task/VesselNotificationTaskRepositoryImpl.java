package pl.plucinski.ais.notification.task;

import com.google.common.collect.Iterables;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.plucinski.ais.notification.VesselNotificationTask;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
class VesselNotificationTaskRepositoryImpl implements VesselNotificationTaskRepository {
    private final static Integer MAX_BATCH_SIZE = 1000;

    private final VesselNotificationTaskJpaRepository vesselNotificationTaskJpaRepository;
    private final VesselNotificationTaskInfrastructureMapper vesselNotificationTaskInfrastructureMapper;

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public List<VesselNotificationTask> findForZoneChecking() {
        List<VesselNotificationTaskEntity> processing = vesselNotificationTaskJpaRepository.findForZoneProcessing();
        List<Long> ids = processing.stream().map(VesselNotificationTaskEntity::getId).collect(Collectors.toList());
        Iterables.partition(ids, MAX_BATCH_SIZE).forEach(vesselNotificationTaskJpaRepository::setProcessingStatus);
        return processing.stream()
                .map(vesselNotificationTaskInfrastructureMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void save(VesselNotificationTask task) {
        VesselNotificationTaskEntity entity = vesselNotificationTaskInfrastructureMapper.map(task);
        vesselNotificationTaskJpaRepository.save(entity);
    }
}
