package pl.plucinski.ais.user;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Entity
@Table(name = "users")
@Builder(toBuilder = true, builderClassName = "Builder")
@RequiredArgsConstructor(onConstructor = @__(@PersistenceConstructor))
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public final class UserEntity {
    private static final String sequence = "users_id_seq";

    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = sequence, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = sequence, sequenceName = sequence)
    private final Long id;

    @CreationTimestamp
    @Column(nullable = false)
    private final LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(nullable = false)
    private final LocalDateTime modificationDate;

    @Column(nullable = false)
    private final String userName;

    @Column(unique = true, nullable = false)
    private final String email;

    @Column(nullable = false)
    private final String password;

    @OneToMany(
            mappedBy = "user",
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private final List<UserRoleEntity> userRoles;
}
