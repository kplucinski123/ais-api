package pl.plucinski.ais.external;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "ais", url = "${ais.ais.url}")
public interface AisClient {
    @GetMapping("openpositions")
    List<PositionDto> getAllPositions();

    @GetMapping("openposition/{mmsi}")
    CoordinatesDto getCoordinates(@PathVariable("mmsi") String mmsi);
}
