package pl.plucinski.ais.vessel.favorite;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
class VesselFavoriteRepositoryImpl implements VesselFavoriteRepository {
    private final VesselFavoriteJpaRepository vesselFavoriteJpaRepository;
    private final VesselFavoriteInfrastructureMapper vesselFavoriteInfrastructureMapper;

    @Override
    public List<VesselFavorite> findAll(Long userId) {
        return vesselFavoriteJpaRepository.findAll(userId).stream()
                .map(vesselFavoriteInfrastructureMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void save(VesselFavorite favorite) {
        VesselFavoriteEntity entity = vesselFavoriteInfrastructureMapper.map(favorite);
        vesselFavoriteJpaRepository.save(entity);
    }

    @Override
    public Optional<VesselFavorite> find(Long userId, Long vesselId) {
        return vesselFavoriteJpaRepository.find(userId, vesselId)
                .map(vesselFavoriteInfrastructureMapper::map);
    }
}
