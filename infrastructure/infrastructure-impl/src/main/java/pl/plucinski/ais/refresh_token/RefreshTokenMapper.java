package pl.plucinski.ais.refresh_token;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pl.plucinski.ais.token.RefreshToken;
import pl.plucinski.ais.user.UserEntityMapper;

@Mapper(componentModel = "spring", uses = UserEntityMapper.class, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface RefreshTokenMapper {
    @Mapping(target = "user", source = "user")
    RefreshToken map(RefreshTokenEntity entity);

    @Mapping(target = "user", source = "user", qualifiedByName = "mapUser")
    @Mapping(target = "modificationDate", ignore = true)
    RefreshTokenEntity map(RefreshToken model);
}
