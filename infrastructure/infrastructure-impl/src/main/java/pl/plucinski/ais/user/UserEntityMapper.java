package pl.plucinski.ais.user;

import com.google.common.collect.Lists;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR
)
public interface UserEntityMapper {

    @Mapping(target = "role", source = "userRoles")
    User map(UserEntity entity);

    @Named("mapUser")
    @Mapping(target = "modificationDate", ignore = true)
    @Mapping(target = "userRoles", ignore = true)
    UserEntity map(User model);

    default String role(List<UserRoleEntity> userRoles) {
        if (userRoles == null) {
            return null;
        }
        return userRoles.get(0).getName();
    }

    default UserEntity mapWithDependencies(User model) {
        UserEntity entity = UserEntity.builder()
                .id(model.getId())
                .creationDate(model.getCreationDate())
                .password(model.getPassword())
                .userName(model.getUserName())
                .email(model.getEmail())
                .userRoles(Lists.newArrayList())
                .build();
        UserRoleEntity userRoleEntity = UserRoleEntity.builder()
                .name(model.getRole())
                .user(entity)
                .build();
        entity.getUserRoles().add(userRoleEntity);
        return entity;
    }

}
