package pl.plucinski.ais.external.token;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(
        onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES))
)
public class RefreshTokenDto {
    @JsonProperty("access_token")
    String accessToken;
    @JsonProperty("expires_in")
    Long expiresIn;
    @JsonProperty("token_type")
    String tokenType;
    String scope;
}
