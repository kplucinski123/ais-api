package pl.plucinski.ais.user;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Entity
@Builder
@Table(name = "user_roles")
@RequiredArgsConstructor(onConstructor = @__(@PersistenceConstructor))
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public final class UserRoleEntity {
    public static final String sequence = "user_roles_id_seq";

    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = sequence, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = sequence, sequenceName = sequence, allocationSize = 1)
    private final Long id;

    @CreationTimestamp
    @Column(nullable = false)
    private final LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(nullable = false)
    private final LocalDateTime modificationDate;

    @Column(nullable = false)
    private final String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private final UserEntity user;
}
