package pl.plucinski.ais.notification;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Repository;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import pl.plucinski.ais.user.User;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Slf4j
@Repository
@RequiredArgsConstructor
@ConditionalOnProperty({
        "spring.mail.username",
        "spring.mail.password",
        "spring.mail.host",
        "spring.mail.port",
})
class EmailNotificationSenderRepositoryImpl implements NotificationSenderRepository {
    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;

    public void send(Notification notification) {
        try {
            Context context = new Context();
            String name = notification.getTask().getVessel().getName();
            User user = notification.getTask().getUser();
            context.setVariable("vessel_name", name);
            String template;
            String subject;
            if (notification.getTask().getStatus() == NotificationTaskStatus.EXPIRED) {
                template = "task_finished";
                subject = "Upłynął termin zadania";
            } else {
                template = "vessel_has_been_located";
                subject = "Zadanie wykonane";
            }
            String body = templateEngine.process(template, context);
            MimeMessage mail = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mail, true);
            helper.setTo(user.getEmail());
            helper.setFrom("AIS App");
            helper.setSubject(subject);
            helper.setText(body, true);
            javaMailSender.send(mail);
        } catch (MessagingException e) {
            log.error("send", e);
        }
    }
}
