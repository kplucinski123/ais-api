package pl.plucinski.ais.external.token;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor(
        onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES))
)
public class GeometryDto {
    private final Double[] coordinates;
}
