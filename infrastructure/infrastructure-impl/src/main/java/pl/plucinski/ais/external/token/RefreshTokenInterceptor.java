package pl.plucinski.ais.external.token;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class RefreshTokenInterceptor implements RequestInterceptor {
    private static final String AUTHORIZATION_HEADER = "Authorization";

    private final RefreshTokenClient refreshTokenClient;
    private final RefreshTokenProperties refreshTokenProperties;

    private LocalDateTime tokenExpire;
    private String token;

    @Override
    public void apply(RequestTemplate requestTemplate) {
        if (token == null || tokenExpire == null || tokenExpire.isBefore(LocalDateTime.now())) {
            LocalDateTime now = LocalDateTime.now();
            RefreshTokenDto tokenDto = refreshTokenClient.generateToken(refreshTokenProperties.getBody());
            tokenExpire = now.plusSeconds(tokenDto.getExpiresIn());
            token = String.format("%s %s", tokenDto.getTokenType(), tokenDto.getAccessToken());
        }
        requestTemplate.header(AUTHORIZATION_HEADER, token);
    }
}
