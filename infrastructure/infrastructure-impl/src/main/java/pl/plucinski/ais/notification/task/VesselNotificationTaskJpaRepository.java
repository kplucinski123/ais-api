package pl.plucinski.ais.notification.task;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.QueryHints;
import org.springframework.data.repository.query.Param;

import javax.persistence.LockModeType;
import javax.persistence.QueryHint;
import java.util.List;

interface VesselNotificationTaskJpaRepository extends JpaRepository<VesselNotificationTaskEntity, Long> {
    @Query("from VesselNotificationTaskEntity entity where entity.status = 'NEW' and entity.nextCheck < current_timestamp")
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    @QueryHints(@QueryHint(name = "javax.persistence.lock.timeout", value = "2000"))
    List<VesselNotificationTaskEntity> findForZoneProcessing();

    @Modifying
    @Query("update VesselNotificationTaskEntity entity set entity.status = 'PROCESSING' where entity.id in :ids")
    void setProcessingStatus(@Param("ids") List<Long> ids);
}
