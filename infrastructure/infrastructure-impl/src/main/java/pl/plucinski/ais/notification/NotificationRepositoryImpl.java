package pl.plucinski.ais.notification;

import com.google.common.collect.Iterables;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
class NotificationRepositoryImpl implements NotificationRepository {
    private final static Integer MAX_BATCH_SIZE = 1000;

    private final NotificationJpaRepository notificationJpaRepository;
    private final NotificationInfrastructureMapper notificationInfrastructureMapper;
    private final List<NotificationSenderRepository> notificationSenderRepositories;

    @Override
    public void save(Notification notification) {
        NotificationEntity entity = notificationInfrastructureMapper.map(notification);
        notificationJpaRepository.save(entity);
    }

    @Override
    @Transactional(Transactional.TxType.REQUIRES_NEW)
    public List<Notification> findForProcessing() {
        List<NotificationEntity> processing = notificationJpaRepository.findForProcessing();
        List<Long> ids = processing.stream().map(NotificationEntity::getId).collect(Collectors.toList());
        Iterables.partition(ids, MAX_BATCH_SIZE).forEach(notificationJpaRepository::setProcessingStatus);
        return processing.stream()
                .map(notificationInfrastructureMapper::map)
                .collect(Collectors.toList());
    }

    @Override
    public void sendNotification(Notification notification) {
        notificationSenderRepositories.forEach(sender -> sender.send(notification));
    }
}
