package pl.plucinski.ais.notification;

interface NotificationSenderRepository {
    /**
     * Method sends notification
     *
     * @param notification {@link Notification notification} for sending
     */
    void send(Notification notification);
}
