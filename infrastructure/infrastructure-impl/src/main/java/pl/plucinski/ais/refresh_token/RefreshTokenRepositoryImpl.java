package pl.plucinski.ais.refresh_token;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import pl.plucinski.ais.token.RefreshToken;
import pl.plucinski.ais.token.RefreshTokenRepository;
import pl.plucinski.ais.token.RefreshTokenStatus;

import java.util.Optional;
import java.util.UUID;

@Repository
@RequiredArgsConstructor
class RefreshTokenRepositoryImpl implements RefreshTokenRepository {
    private final RefreshTokenMapper refreshTokenMapper;
    private final RefreshTokenJpaRepository refreshTokenJpaRepository;

    @Override
    public void saveToken(RefreshToken refreshToken) {
        RefreshTokenEntity entity = refreshTokenMapper.map(refreshToken);
        refreshTokenJpaRepository.save(entity);
    }

    @Override
    public void changeTokenStatus(UUID uuid, RefreshTokenStatus status) {
        refreshTokenJpaRepository.findById(uuid)
                .map(RefreshTokenEntity::toBuilder)
                .map(token -> token.status(status))
                .map(RefreshTokenEntity.Builder::build)
                .ifPresent(refreshTokenJpaRepository::save);
    }

    @Override
    public Optional<RefreshToken> getToken(UUID uuid) {
        return refreshTokenJpaRepository.findById(uuid)
                .map(refreshTokenMapper::map);
    }
}
