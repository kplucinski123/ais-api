package pl.plucinski.ais.refresh_token;

import org.springframework.data.repository.CrudRepository;

import java.util.UUID;

interface RefreshTokenJpaRepository extends CrudRepository<RefreshTokenEntity, UUID> {
}
