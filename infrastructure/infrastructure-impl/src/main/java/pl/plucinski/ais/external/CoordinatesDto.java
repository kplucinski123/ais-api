package pl.plucinski.ais.external;

import com.fasterxml.jackson.annotation.JsonCreator;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import pl.plucinski.ais.external.token.GeometryDto;

@Getter
@RequiredArgsConstructor(
        onConstructor = @__(@JsonCreator(mode = JsonCreator.Mode.PROPERTIES))
)
public class CoordinatesDto {
    public final GeometryDto geometry;
}
