package pl.plucinski.ais.vessel;

import org.springframework.data.jpa.repository.JpaRepository;

interface VesselJpaRepository extends JpaRepository<VesselEntity, Long> {
}
