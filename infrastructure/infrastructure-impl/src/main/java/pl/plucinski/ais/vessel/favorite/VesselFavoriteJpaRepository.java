package pl.plucinski.ais.vessel.favorite;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

interface VesselFavoriteJpaRepository extends JpaRepository<VesselFavoriteEntity, Long> {
    @Query("from VesselFavoriteEntity entity where entity.user.id = :userId and entity.vessel.id = :vesselId")
    Optional<VesselFavoriteEntity> find(@Param("userId") Long userId, @Param("vesselId") Long vesselId);

    @Query("from VesselFavoriteEntity entity where entity.user.id = :userId")
    List<VesselFavoriteEntity> findAll(@Param("userId") Long userId);
}
