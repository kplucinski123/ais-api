package pl.plucinski.ais.vessel;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Entity
@Table(name = "vessels")
@Builder(toBuilder = true, builderClassName = "Builder")
@RequiredArgsConstructor(onConstructor = @__(@PersistenceConstructor))
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class VesselEntity {
    private static final String sequence = "vessels_id_seq";

    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = sequence, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = sequence, sequenceName = sequence)
    private final Long id;

    @CreationTimestamp
    @Column(nullable = false)
    private final LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(nullable = false)
    private final LocalDateTime modificationDate;

    @Column(nullable = false, unique = true)
    private final String mmsi;

    @Column(nullable = false)
    private final Integer shipType;

    @Column(nullable = false)
    private final String country;

    private final String name;
}
