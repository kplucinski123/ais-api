package pl.plucinski.ais.notification.task;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.annotation.PersistenceConstructor;
import pl.plucinski.ais.notification.NotificationTaskStatus;
import pl.plucinski.ais.user.UserEntity;
import pl.plucinski.ais.vessel.VesselEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Entity
@Builder(toBuilder = true)
@Table(name = "vessel_notification_tasks")
@RequiredArgsConstructor(onConstructor = @__(@PersistenceConstructor))
@NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
public class VesselNotificationTaskEntity {
    private static final String sequence = "vessel_notification_tasks_id_seq";

    @Id
    @Column(nullable = false)
    @GeneratedValue(generator = sequence, strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(allocationSize = 1, name = sequence, sequenceName = sequence)
    private final Long id;

    @CreationTimestamp
    @Column(nullable = false)
    private final LocalDateTime creationDate;

    @UpdateTimestamp
    @Column(nullable = false)
    private final LocalDateTime modificationDate;

    @Column(nullable = false)
    private final LocalDateTime deadLine;

    @Column(nullable = false)
    private final LocalDateTime nextCheck;

    @Column(nullable = false)
    private final Double maxLatitude;

    @Column(nullable = false)
    private final Double minLatitude;

    @Column(nullable = false)
    private final Double maxLongitude;

    @Column(nullable = false)
    private final Double minLongitude;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private final NotificationTaskStatus status;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private final UserEntity user;

    @ManyToOne
    @JoinColumn(name = "vessel_id")
    private final VesselEntity vessel;
}
