package pl.plucinski.ais.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface UserJpaRepository extends JpaRepository<UserEntity, Long> {
    @Query("from UserEntity user where user.email = :email")
    Optional<UserEntity> findByEmail(@Param("email") String email);
}
