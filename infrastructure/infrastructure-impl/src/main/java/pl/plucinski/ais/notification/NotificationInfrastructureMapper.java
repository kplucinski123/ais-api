package pl.plucinski.ais.notification;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
interface NotificationInfrastructureMapper {
    Notification map(NotificationEntity entity);

    @Mapping(target = "modificationDate", ignore = true)
    NotificationEntity map(Notification model);
}
