package pl.plucinski.ais.vessel.favorite;

import java.util.List;
import java.util.Optional;

public interface VesselFavoriteRepository {
    /**
     * Method returns all favorites for user
     *
     * @param userId {@link Long user identifier}
     *
     * @return {@link List list} of {@link VesselFavorite favorites}
     */
    List<VesselFavorite> findAll(Long userId);

    /**
     * Method save vessel favorite
     *
     * @param favorite {@link VesselFavorite favorite} for saving
     */
    void save(VesselFavorite favorite);

    /**
     * Method returns vessel favorite by vessel identifier and user identifier
     *
     * @param userId {@link Long user identifier}
     * @param vesselId {@link Long vessel identifier}
     *
     * @return {@link Optional optional} of {@link VesselFavorite favorite}
     */
    Optional<VesselFavorite> find(Long userId, Long vesselId);
}
