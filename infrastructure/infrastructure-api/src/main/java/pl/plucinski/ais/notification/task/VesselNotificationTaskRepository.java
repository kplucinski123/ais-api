package pl.plucinski.ais.notification.task;

import pl.plucinski.ais.notification.VesselNotificationTask;

import java.util.List;

interface VesselNotificationTaskRepository {

    /**
     * Method returns notification tasks and changing status to PROCESSING
     *
     * @return {@link List list} of {@link VesselNotificationTask tasks}
     */
    List<VesselNotificationTask> findForZoneChecking();

    /**
     * Method saves task
     *
     * @param task {@link VesselNotificationTask task for saving}
     */
    void save(VesselNotificationTask task);
}
