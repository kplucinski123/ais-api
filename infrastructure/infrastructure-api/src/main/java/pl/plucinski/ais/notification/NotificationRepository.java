package pl.plucinski.ais.notification;

import java.util.List;

public interface NotificationRepository {
    /**
     * Method saves notification
     *
     * @param notification {@link Notification notification} for saving
     */
    void save(Notification notification);

    /**
     * Method return notification for processing
     *
     * @return {@link List list} of {@link Notification notifications}
     */
    List<Notification> findForProcessing();

    /**
     * Method sends notification
     *
     * @param notification {@link Notification notification} for sending
     */
    void sendNotification(Notification notification);
}
