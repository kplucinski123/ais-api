package pl.plucinski.ais.user;

import java.util.Optional;

public interface UserRepository {

    /**
     * Method saves user
     *
     * @param user {@link User} for save
     *
     * @return saved {@link User user}
     */
    User save(User user);

    /**
     * Method returns user by email
     *
     * @param email {@link String} user email
     *
     * @return found {@link Optional optional} of {@link User user}
     */
    Optional<User> findUserByEmail(String email);

    /**
     * Method returns user by identifier
     *
     * @param id {@link Long} user identifier
     *
     * @return found {@link Optional optional} of {@link User user}
     */
    Optional<User> findUserById(Long id);
}
