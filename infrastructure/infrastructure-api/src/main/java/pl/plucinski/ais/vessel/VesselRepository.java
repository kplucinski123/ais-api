package pl.plucinski.ais.vessel;

import java.util.List;
import java.util.Optional;

public interface VesselRepository {
    /**
     * Method synchronizes vessels from external resource
     */
    void synchronize();

    /**
     * Method returns all vessels
     *
     * @return {@link List list} of {@link Vessel vessels}
     */
    List<Vessel> getAll();

    /**
     * Method returns vessel by id
     *
     * @param id {@link Long vessel identifier}
     *
     * @return {@link Optional optional} of {@link Vessel vessel}
     */
    Optional<Vessel> findById(Long id);

    /**
     * Method returns vessel details
     *
     * @param vessel {@link Vessel vessel}
     *
     * @return {@link VesselDetails details}
     */
    VesselDetails getDetails(Vessel vessel);

    /**
     * Method returns vessel coordinates
     *
     * @param vessel {@link Vessel vessel}
     */
    VesselCoordinates getCoordinates(Vessel vessel);
}
