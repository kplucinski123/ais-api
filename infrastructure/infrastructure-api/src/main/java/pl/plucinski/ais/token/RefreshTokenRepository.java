package pl.plucinski.ais.token;

import java.util.Optional;
import java.util.UUID;

public interface RefreshTokenRepository {
    /**
     * Method saves refresh token
     *
     * @param refreshToken {@link RefreshToken} refresh token for save
     */
    void saveToken(RefreshToken refreshToken);

    /**
     * Method changes refresh token status
     *
     * @param uuid {@link UUID} refresh token uuid
     * @param status {@link RefreshTokenStatus} status for save
     */
    void changeTokenStatus(UUID uuid, RefreshTokenStatus status);

    /**
     * Method returns optional of refresh token
     *
     * @param uuid {@link UUID} refresh token uuid
     *
     * @return {@link Optional optional} of {@link RefreshToken refresh token}
     */
    Optional<RefreshToken> getToken(UUID uuid);
}
