# AIS Api

## O aplikacji

Aplikacja służy do monitorowania jednostek morskich oraz ustawienia powiadomienia mailowego gdy jednosta wpłynie w wybrany przez nas obszar.
Aplikacja podzielona jest na repozytorium frontend i backend.

##### Repozytorium frontendowe dla aplikacji pod adresem:
```
https://gitlab.com/kplucinski123/ais-app
```

##### Uruchomienie:
Należy skopiować repozytorium (```https://gitlab.com/kplucinski123/ais-api```).
W pliku ```application/src/main/resources/application.yaml``` należy uzupełnić konfigurację dla wysyłki maili:
```
spring:
  mail:
    host: 
    port: 
    username: 
    password: 
```
W przypadku braku konfiguracji, aplikacja jedynie zapisze powiadomnienie w bazie danych (tabela notifications)
Następnie z katalogu głównego wykonać polecenie:
```docker-compose up```

##### Dokumentacja API:
Dokumentacja generowana przez OpenApi. Adres:
```
http://localhost:7777/swagger-ui
```

##### Wykorzystane biblioteki:
- Spring Data JPA - zapytania do bazy danych
- Hibernate - mapowanie ORM
- Spring Boot Email - wysyłanie powiedamionia o zlokalizowanym statku w zdefiniowanym obszarze
- Shedlock - zapewnienie pojedyńczego uruchamiania schedulera dla wieloinstancyjności
- Liquibase - wersjonowanie bazy danych
- Openfeign - zapytania do zewnętrznych serwisów
- OpenApi - dokumentacja API
- Thymeleaf - szablon wysyłanych maili
- Lombok - zmniejszenie ilości kofu
- Mapstruct - mapowanie obiektów
- Spock Groovy - testy
