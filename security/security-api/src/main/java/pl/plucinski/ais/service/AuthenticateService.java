package pl.plucinski.ais.service;

import pl.plucinski.ais.model.LoginUser;
import pl.plucinski.ais.model.RegisterUser;
import pl.plucinski.ais.model.RegisteredUser;
import pl.plucinski.ais.model.TokenUser;

import java.util.UUID;

public interface AuthenticateService {

    /**
     * Method creates new user
     *
     * @param registerUser {@link RegisteredUser} user for creation
     * @return {@link RegisteredUser created user}
     */
    RegisteredUser register(RegisterUser registerUser);

    /**
     * Method create and returns token for user
     *
     * @param registerUser {@link LoginUser} user for login
     * @return logged user's {@link TokenUser token}
     */
    TokenUser login(LoginUser registerUser);

    /**
     * Method refresh token
     *
     * @param token {@link UUID} refresh token uuid
     * @return logged user's {@link TokenUser token}
     */
    TokenUser refreshToken(UUID token);

    /**
     * Method validates token
     *
     * @param tokenHeader token for validation
     */
    void validateToken(String tokenHeader);
}
