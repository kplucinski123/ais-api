package pl.plucinski.ais.model;

public interface RegisterUser {
    String getEmail();
    String getUserName();
    String getPassword();
}
