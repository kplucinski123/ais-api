package pl.plucinski.ais.model;

import org.springframework.security.core.userdetails.UserDetails;

public interface LoggedUser extends UserDetails {
    Long getId();
    String getEmail();
}
