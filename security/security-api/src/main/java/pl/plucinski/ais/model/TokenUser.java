package pl.plucinski.ais.model;

public interface TokenUser {
    Long getUserId();
    String getToken();
}
