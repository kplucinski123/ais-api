package pl.plucinski.ais.model;

public interface LoginUser {
    String getEmail();
    String getPassword();
}
