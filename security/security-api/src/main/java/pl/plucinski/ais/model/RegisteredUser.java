package pl.plucinski.ais.model;

public interface RegisteredUser {
    Long getId();
    String getEmail();
}
