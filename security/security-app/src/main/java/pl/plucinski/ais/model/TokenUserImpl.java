package pl.plucinski.ais.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class TokenUserImpl implements TokenUser {
    private final Long userId;
    private final String token;
}
