package pl.plucinski.ais;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pl.plucinski.ais.mapper.UserAuthenticateMapper;
import pl.plucinski.ais.model.LoginUser;
import pl.plucinski.ais.model.RegisterUser;
import pl.plucinski.ais.model.RegisteredUser;
import pl.plucinski.ais.model.TokenUser;
import pl.plucinski.ais.model.TokenUserImpl;
import pl.plucinski.ais.model.UserDetailsModel;
import pl.plucinski.ais.refresh_token.RefreshTokenService;
import pl.plucinski.ais.service.AuthenticateService;
import pl.plucinski.ais.service.TokenService;
import pl.plucinski.ais.token.RefreshToken;
import pl.plucinski.ais.user.User;
import pl.plucinski.ais.user.UserService;

import java.util.UUID;

@Service
@RequiredArgsConstructor
class AuthenticateServiceImpl implements AuthenticateService {
    private final UserService userService;
    private final TokenService tokenService;
    private final AuthenticationManager authenticationManager;
    private final UserAuthenticateMapper userAuthenticateMapper;
    private final RefreshTokenService refreshTokenService;

    @Override
    public RegisteredUser register(RegisterUser registerUser) {
        if (userService.checkUserExists(registerUser.getEmail())) {
            throw new BusinessException(BusinessExceptionCode.USER_EXISTS);
        }
        User model = userAuthenticateMapper.map(registerUser);
        model = userService.save(model);
        return userAuthenticateMapper.map(model);
    }

    @Override
    public TokenUser login(LoginUser loginUser) {
        SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginUser.getEmail(), loginUser.getPassword())
        ));
        User user = userService.findByEmail(loginUser.getEmail());
        return getTokenUser(user);
    }

    @Override
    public TokenUser refreshToken(UUID token) {
        return refreshTokenService.get(token)
                .map(RefreshToken::getUser)
                .map(this::getTokenUser)
                .orElseThrow(RuntimeException::new);
    }

    @Override
    public void validateToken(String tokenHeader) {
        User user = tokenService.getUsernameFromToken(tokenHeader)
                .map(userService::findByEmail)
                .orElseThrow(() -> new BusinessException(BusinessExceptionCode.INVALID_TOKEN));
        UserDetailsModel userDetails = userAuthenticateMapper.modelToDetails(user);
        if (!tokenService.validateToken(tokenHeader, userDetails)) {
            throw new BusinessException(BusinessExceptionCode.INVALID_TOKEN);
        }
    }

    private TokenUser getTokenUser(User user) {
        UserDetails userDetails = userAuthenticateMapper.modelToDetails(user);
        String token = tokenService.generateToken(userDetails);
        return TokenUserImpl.builder()
                .userId(user.getId())
                .token(token)
                .build();
    }
}
