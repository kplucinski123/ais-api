package pl.plucinski.ais.service;

import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public interface TokenService {
    /**
     * Method returns user name
     *
     * @param token user {@link String token}
     *
     * @return {@link Optional optional} of {@link String user name}
     */
    Optional<String> getUsernameFromToken(String token);

    /**
     * Method validates user token
     *
     * @param token user {@link String token}
     * @param user {@link UserDetails logged user}
     *
     * @return {@link Boolean result of validation}
     */
    Boolean validateToken(String token, UserDetails user);

    /**
     * Method generates token for user
     *
     * @param userDetails {@link UserDetails user}
     *
     * @return {@link String generated token}
     */
    String generateToken(UserDetails userDetails);
}
