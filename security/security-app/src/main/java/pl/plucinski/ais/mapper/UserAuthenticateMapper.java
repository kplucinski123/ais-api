package pl.plucinski.ais.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import pl.plucinski.ais.model.RegisterUser;
import pl.plucinski.ais.model.RegisteredUserImpl;
import pl.plucinski.ais.model.UserDetailsModel;
import pl.plucinski.ais.user.User;

@Mapper(componentModel = "spring")
public interface UserAuthenticateMapper {

    BCryptPasswordEncoder ENCODER = new BCryptPasswordEncoder();

    @Mapping(source = "password", target = "password", qualifiedByName = "encodePassword")
    @Mapping(constant = "ROLE_USER", target = "role")
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    User map(RegisterUser user);

    RegisteredUserImpl map(User user);

    UserDetailsModel modelToDetails(User user);

    @Named("encodePassword")
    default String encodePassword(String password) {
        return ENCODER.encode(password);
    }
}
