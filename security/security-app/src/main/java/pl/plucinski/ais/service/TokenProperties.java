package pl.plucinski.ais.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
@ConfigurationProperties(prefix = "ais.token")
class TokenProperties {
    private String secret;
    private Long expiration;
}
