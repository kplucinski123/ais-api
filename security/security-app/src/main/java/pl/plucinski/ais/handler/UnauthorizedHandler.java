package pl.plucinski.ais.handler;

import org.springframework.security.web.AuthenticationEntryPoint;

public interface UnauthorizedHandler extends AuthenticationEntryPoint {
}
