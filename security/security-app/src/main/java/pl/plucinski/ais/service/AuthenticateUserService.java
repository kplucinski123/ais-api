package pl.plucinski.ais.service;

import org.springframework.security.core.userdetails.UserDetailsService;

public interface AuthenticateUserService extends UserDetailsService {
}
