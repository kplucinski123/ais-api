package pl.plucinski.ais.service;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.ClassUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.plucinski.ais.Unsecured;
import pl.plucinski.ais.model.Endpoint;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
class SecurityServiceImpl implements SecurityService {
    private final ApplicationContext applicationContext;

    @Override
    public List<Endpoint> findUnsecuredEndpoints() {
        return applicationContext.getBeansWithAnnotation(RestController.class).values().stream()
                .map(ClassUtils::getUserClass)
                .map(Class::getInterfaces)
                .flatMap(Arrays::stream)
                .map(Class::getDeclaredMethods)
                .flatMap(Arrays::stream)
                .filter(method -> method.isAnnotationPresent(Unsecured.class))
                .map(this::findEndpoints)
                .flatMap(Collection::stream)
                .distinct()
                .collect(Collectors.toList());
    }

    private List<Endpoint> findEndpoints(Method method) {
        return Stream.of(method.getDeclaringClass().getDeclaredAnnotation(RequestMapping.class).value())
                .map(this::insertSlashCharacter)
                .map(path -> findEndpoints(path, method))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<Endpoint> findEndpoints(String path, Method method) {
        List<Endpoint> endpoints = Lists.newArrayList();
        if (method.getDeclaredAnnotation(GetMapping.class) != null) {
            List<Endpoint> getEndpoints = Optional.ofNullable(method.getDeclaredAnnotation(GetMapping.class))
                    .map(GetMapping::value)
                    .map(values -> createEndpoints(HttpMethod.GET, path, values))
                    .orElseGet(() -> createEndpoints(HttpMethod.GET, path));
            endpoints.addAll(getEndpoints);
        }
        if (method.getDeclaredAnnotation(PostMapping.class) != null) {
            List<Endpoint> postPairs = Optional.ofNullable(method.getDeclaredAnnotation(PostMapping.class))
                    .map(PostMapping::value)
                    .map(values -> createEndpoints(HttpMethod.POST, path, values))
                    .orElseGet(() -> createEndpoints(HttpMethod.POST, path));
            endpoints.addAll(postPairs);
        }
        if (method.getDeclaredAnnotation(DeleteMapping.class) != null) {
            List<Endpoint> deletePairs = Optional.ofNullable(method.getDeclaredAnnotation(DeleteMapping.class))
                    .map(DeleteMapping::value)
                    .map(values -> createEndpoints(HttpMethod.DELETE, path, values))
                    .orElseGet(() -> createEndpoints(HttpMethod.DELETE, path));
            endpoints.addAll(deletePairs);
        }
        if (method.getDeclaredAnnotation(PatchMapping.class) != null) {
            List<Endpoint> patchPairs = Optional.ofNullable(method.getDeclaredAnnotation(PatchMapping.class))
                    .map(PatchMapping::value)
                    .map(values -> createEndpoints(HttpMethod.PATCH, path, values))
                    .orElseGet(() -> createEndpoints(HttpMethod.PATCH, path));
            endpoints.addAll(patchPairs);
        }
        if (method.getDeclaredAnnotation(PutMapping.class) != null) {
            List<Endpoint> putPairs = Optional.ofNullable(method.getDeclaredAnnotation(PutMapping.class))
                    .map(PutMapping::value)
                    .map(values -> createEndpoints(HttpMethod.PUT, path, values))
                    .orElseGet(() -> createEndpoints(HttpMethod.PUT, path));
            endpoints.addAll(putPairs);
        }
        return endpoints;
    }

    private List<Endpoint> createEndpoints(HttpMethod httpMethod, String path, String... paths) {
        List<Endpoint> endpoints = Lists.newArrayList();
        List<String> collect = Stream.of(paths)
                .filter(StringUtils::isNotBlank)
                .collect(Collectors.toList());
        if (CollectionUtils.isNotEmpty(collect)) {
            collect.stream()
                    .map(suffix -> addSuffix(path, suffix))
                    .map(c -> createEndpoint(httpMethod, c))
                    .forEach(endpoints::add);
        } else {
            endpoints.add(createEndpoint(httpMethod, path));
        }
        return endpoints;
    }

    private Endpoint createEndpoint(HttpMethod httpMethod, String path) {
        return Endpoint.builder()
                .method(httpMethod)
                .path(path)
                .build();
    }

    private String addSuffix(String path, String suffix) {
        if (!path.endsWith("/")) {
            path += "/";
        }
        return path + suffix;
    }

    private String insertSlashCharacter(String path) {
        if (!path.startsWith("/")) {
            return "/" + path;
        }
        return path;
    }
}
