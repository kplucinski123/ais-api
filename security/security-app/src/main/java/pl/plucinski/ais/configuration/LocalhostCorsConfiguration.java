package pl.plucinski.ais.configuration;

import com.google.common.collect.Lists;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.web.cors.CorsConfiguration;

import javax.annotation.PostConstruct;

@Profile("local")
@Component
public class LocalhostCorsConfiguration extends CorsConfiguration {
    @PostConstruct
    public void init() {
        setAllowedOrigins(Lists.newArrayList("http://localhost:4200"));
        setAllowedMethods(Lists.newArrayList("GET", "POST", "PUT", "DELETE", "PATCH"));
        setAllowCredentials(true);
        setAllowedHeaders(Lists.newArrayList("X-Requested-With", "token", "content-type", "Set-Cookie"));
    }
}
