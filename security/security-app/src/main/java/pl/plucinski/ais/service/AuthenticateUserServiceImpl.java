package pl.plucinski.ais.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.plucinski.ais.mapper.UserAuthenticateMapper;
import pl.plucinski.ais.user.UserService;

@Service
@RequiredArgsConstructor
public class AuthenticateUserServiceImpl implements AuthenticateUserService {

    private final UserService userService;
    private final UserAuthenticateMapper userAuthenticateMapper;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userAuthenticateMapper.modelToDetails(userService.findByEmail(username));
    }
}
