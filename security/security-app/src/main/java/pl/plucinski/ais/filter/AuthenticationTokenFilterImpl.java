package pl.plucinski.ais.filter;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import pl.plucinski.ais.mapper.UserAuthenticateMapper;
import pl.plucinski.ais.model.UserDetailsModel;
import pl.plucinski.ais.service.TokenService;
import pl.plucinski.ais.user.UserService;
import pl.plucinski.ais.user.User;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;
import java.util.Optional;

@Log4j2
@Component
@RequiredArgsConstructor
class AuthenticationTokenFilterImpl extends OncePerRequestFilter implements AuthenticationTokenFilter {
    private final UserService userService;
    private final TokenService tokenService;
    private final UserAuthenticateMapper userAuthenticateMapper;

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        String authToken = request.getHeader("token");
        if (StringUtils.isNotBlank(authToken)) {
            try {
                Optional<String> username = tokenService.getUsernameFromToken(authToken);
                if (Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
                    username.ifPresent(name -> validToken(request, authToken, name));
                }
            } catch (Exception exception) {
                log.error(exception);
            }
        }
        chain.doFilter(request, response);
    }

    private void validToken(HttpServletRequest request, String authToken, String username) {
        User user = this.userService.findByEmail(username);
        UserDetailsModel userDetails = userAuthenticateMapper.modelToDetails(user);
        if (tokenService.validateToken(authToken, userDetails)) {
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }
}
