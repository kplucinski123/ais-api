package pl.plucinski.ais.model;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class RegisteredUserImpl implements RegisteredUser {
    private final Long id;
    private final String email;
}
