package pl.plucinski.ais.service;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import pl.plucinski.ais.BusinessException;
import pl.plucinski.ais.BusinessExceptionCode;

import java.util.Date;
import java.util.Optional;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
class TokenServiceImpl implements TokenService {
    private final TokenProperties tokenProperties;

    @Override
    public Optional<String> getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    @Override
    public Boolean validateToken(String token, UserDetails user) {
        Optional<String> username = getUsernameFromToken(token);
        return username.orElse(StringUtils.EMPTY).equals(user.getUsername()) && !isTokenExpired(token);
    }

    @Override
    public String generateToken(UserDetails userDetails) {
        Date createdDate = new Date();
        Date expirationDate = calculateExpirationDate(createdDate);
        return Jwts.builder()
                .setSubject(userDetails.getUsername())
                .setIssuedAt(createdDate)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, tokenProperties.getSecret())
                .compact();
    }

    private Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration)
                .orElseThrow(() -> new BusinessException(BusinessExceptionCode.USER_NOT_FOUND));
    }

    private <T> Optional<T> getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return Optional.ofNullable(claimsResolver.apply(claims));
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setAllowedClockSkewSeconds(360000000)
                .setSigningKey(tokenProperties.getSecret())
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token) {
        Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private Date calculateExpirationDate(Date createdDate) {
        return new Date(createdDate.getTime() + tokenProperties.getExpiration() * 1000);
    }
}
