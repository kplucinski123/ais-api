package pl.plucinski.ais.configuration;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import pl.plucinski.ais.filter.AuthenticationTokenFilter;
import pl.plucinski.ais.handler.UnauthorizedHandler;
import pl.plucinski.ais.model.Endpoint;
import pl.plucinski.ais.service.SecurityService;

import java.util.List;
import java.util.Optional;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private static final String[] API_DOCS_ENDPOINTS = {"/swagger-ui.html", "/v3/api-docs/**","/swagger-ui/**"};

    private final UnauthorizedHandler unauthorizedHandler;
    private final UserDetailsService userDetailsService;
    private final AuthenticationTokenFilter authenticationTokenFilter;
    private final SecurityService securityService;
    private final Optional<LocalhostCorsConfiguration> corsConfiguration;

    @Autowired
    public void configureAuthentication(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .userDetailsService(this.userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry = httpSecurity.csrf().disable()
                .authorizeRequests();
        List<Endpoint> unsecuredEndpoints = securityService.findUnsecuredEndpoints();
        unsecuredEndpoints.add(Endpoint.builder()
                .path("/ws/info/**")
                .method(HttpMethod.GET)
                .build());
        unsecuredEndpoints.add(Endpoint.builder()
                .path("/ws")
                .method(HttpMethod.GET)
                .build());
        unsecuredEndpoints.add(Endpoint.builder()
                .path("/ws/**")
                .method(HttpMethod.GET)
                .build());
        unsecuredEndpoints.forEach(endpoint -> expressionInterceptUrlRegistry.antMatchers(endpoint.getMethod(), endpoint.getPath()).permitAll());
        unsecuredEndpoints.stream()
                .map(Endpoint::getPath)
                .distinct()
                .forEach(path -> expressionInterceptUrlRegistry.antMatchers(HttpMethod.OPTIONS, path).permitAll());
        expressionInterceptUrlRegistry.antMatchers(API_DOCS_ENDPOINTS).permitAll();
        expressionInterceptUrlRegistry.anyRequest().authenticated();
        httpSecurity.csrf().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).accessDeniedHandler((httpServletRequest, httpServletResponse, e) -> {}).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        httpSecurity.addFilterBefore(authenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
        httpSecurity.headers().frameOptions().sameOrigin().cacheControl();
        httpSecurity.cors().configurationSource(c -> corsConfiguration.orElse(null));
    }
}
