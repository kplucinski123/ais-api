package pl.plucinski.ais.model;

import lombok.Builder;
import lombok.Getter;
import org.springframework.http.HttpMethod;

@Getter
@Builder
public class Endpoint {
    private final String path;
    private final HttpMethod method;
}
