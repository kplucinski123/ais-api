package pl.plucinski.ais.service;

import pl.plucinski.ais.model.Endpoint;

import java.util.List;

public interface SecurityService {
    /**
     * Method returns all unsecured endpoints
     *
     * @return {@link List list} of {@link Endpoint unsecured endpoints}
     */
    List<Endpoint> findUnsecuredEndpoints();
}
