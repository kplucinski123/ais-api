FROM maven:3.6.3-openjdk-11 AS builder

WORKDIR /app

COPY . /app/

RUN mvn clean package -U

FROM openjdk:11-jre-slim

WORKDIR /app

EXPOSE 7777
COPY --from=builder /app/application/target/ais-api.jar ais-api.jar

ENTRYPOINT ["java", "-jar", "ais-api.jar"]
