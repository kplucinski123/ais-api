package pl.plucinski.ais.util

import org.springframework.security.core.GrantedAuthority
import pl.plucinski.ais.model.LoggedUser
import pl.plucinski.ais.user.User

class TestLoggedUser implements LoggedUser {
    private User user

    TestLoggedUser(User user) {
        this.user = user
    }

    @Override
    Long getId() {
        user.id
    }

    @Override
    String getEmail() {
        user.id
    }

    @Override
    Collection<? extends GrantedAuthority> getAuthorities() {
        []
    }

    @Override
    String getPassword() {
        user.password
    }

    @Override
    String getUsername() {
        null
    }

    @Override
    boolean isAccountNonExpired() {
        false
    }

    @Override
    boolean isAccountNonLocked() {
        false
    }

    @Override
    boolean isCredentialsNonExpired() {
        false
    }

    @Override
    boolean isEnabled() {
        false
    }
}
