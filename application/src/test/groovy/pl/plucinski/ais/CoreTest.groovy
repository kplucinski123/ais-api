package pl.plucinski.ais

import org.apache.http.client.HttpClient
import org.apache.http.impl.client.HttpClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpHeaders
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory
import org.springframework.web.client.RestTemplate
import pl.plucinski.ais.user.User
import pl.plucinski.ais.helper.SecurityHelper
import pl.plucinski.ais.helper.UserHelper
import spock.lang.Shared
import spock.lang.Specification

@AutoConfigureMockMvc
@SpringBootTest(classes = Application, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CoreTest extends Specification {

    @Autowired UserHelper userHelper
    @Autowired TestRestTemplate testRestTemplate
    @Autowired SecurityHelper securityHelper
    @Shared RestTemplate restTemplate
    @Shared HttpHeaders userHeaders
    @Shared User user

    def setup() {
        restTemplate = testRestTemplate.getRestTemplate()
        HttpClient httpClient = HttpClientBuilder.create().build()
        restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));
        setHeaders()
    }

    def cleanup() {
        userHelper.clean()
    }

    def setHeaders() {
        String userName = 'test'
        String userEmail = 'test@test.pl'
        String userPassword = 'test'
        user = userHelper.user(userPassword, userName, userEmail)
        userHeaders = securityHelper.securityHeader(userEmail, userPassword)
    }

}
