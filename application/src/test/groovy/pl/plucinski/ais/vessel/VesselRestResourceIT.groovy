package pl.plucinski.ais.vessel

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.test.context.jdbc.Sql
import org.springframework.web.util.UriComponentsBuilder
import pl.plucinski.ais.CoreTest
import pl.plucinski.ais.helper.VesselHelper

import static org.springframework.test.context.jdbc.Sql.ExecutionPhase.AFTER_TEST_METHOD

@Sql(scripts = '/db/delete_all.sql', executionPhase = AFTER_TEST_METHOD)
class VesselRestResourceIT extends CoreTest {
    @Autowired VesselHelper vesselHelper

    def "should return all vessels"() {
        given:
        List<Vessel> vessels = (1..30).collect {vesselHelper.vessel("${it}", "name${it}")}
        String url = UriComponentsBuilder.fromPath('/vessels').build()
        HttpEntity payload = new HttpEntity<>(userHeaders)

        when:
        ResponseEntity<VesselsResponse> response = restTemplate.exchange(url, HttpMethod.GET, payload, VesselsResponse)

        then:
        response.statusCode == HttpStatus.OK
        response.body
        VesselsResponse vesselsResponse = response.body
        vesselsResponse.vessels
        vesselsResponse.vessels.size() == vessels.size()
    }

    def "should return unauthorized when try get vessels without login"() {
        given:
        String url = UriComponentsBuilder.fromPath('/vessels').build()
        HttpEntity payload = new HttpEntity<>(HttpHeaders.EMPTY)

        when:
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.GET, payload, Void)

        then:
        response.statusCode == HttpStatus.UNAUTHORIZED
    }

    def "should return unauthorized when try get vessel details without login"() {
        given:
        String url = UriComponentsBuilder.fromPath('/vessels/{id}').buildAndExpand(1)
        HttpEntity payload = new HttpEntity<>(HttpHeaders.EMPTY)

        when:
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.GET, payload, Void)

        then:
        response.statusCode == HttpStatus.UNAUTHORIZED
    }

    def "should add vessel to favorite"() {
        given:
        Vessel vessel = vesselHelper.vessel("1", "name")
        String url = UriComponentsBuilder.fromPath('/vessels/{vesselId}/favorite')
                .buildAndExpand(vessel.id)
        HttpEntity payload = new HttpEntity<>(userHeaders)

        when:
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, payload, Void)

        then:
        response.statusCode == HttpStatus.NO_CONTENT
    }

    def "should return error when try add non exists vessel to favorite"() {
        given:
        String url = UriComponentsBuilder.fromPath('/vessels/{vesselId}/favorite')
                .buildAndExpand(1)
        HttpEntity payload = new HttpEntity<>(userHeaders)

        when:
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, payload, Void)

        then:
        response.statusCode == HttpStatus.BAD_REQUEST
    }

    def "should return unauthorized when try add vessel to favorite"() {
        given:
        Vessel vessel = vesselHelper.vessel("1", "name")
        String url = UriComponentsBuilder.fromPath('/vessels/{vesselId}/favorite')
                .buildAndExpand(vessel.id)
        HttpEntity payload = new HttpEntity<>(HttpHeaders.EMPTY)

        when:
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, payload, Void)

        then:
        response.statusCode == HttpStatus.UNAUTHORIZED
    }
}
