package pl.plucinski.ais.helper

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Component
import pl.plucinski.ais.user.User

import javax.sql.DataSource
import java.time.LocalDateTime

@Component
class UserHelper {
    @Autowired DataSource dataSource
    @Autowired PasswordEncoder passwordEncoder

    def clean() {
        Sql sql = new Sql(dataSource)
        String deleteRole = 'delete from user_roles'
        sql.execute(deleteRole)
        String deleteUser = 'delete from users'
        sql.execute(deleteUser)
    }

    User user(String password, String userName, String email) {
        Sql sql = new Sql(dataSource)
        String userRole = 'ROLE_USER'
        String encodedPassword = passwordEncoder.encode(password)
        User user = User.builder()
                .userName(userName)
                .email(email)
                .password(encodedPassword)
                .role(userRole)
                .build()
        LocalDateTime now = LocalDateTime.now()
        GString insertUser = """
            insert into users (id, creation_date, modification_date, user_name, email, password)
            values (nextval('users_id_seq'), ${now}, ${now}, ${userName}, ${email}, ${encodedPassword})
        """
        GroovyRowResult[] results = sql.executeInsert(insertUser, ['id'])
        Long id = (Long) results[0].get('id')
        GString insertRole = """
            insert into user_roles (id, creation_date, modification_date, name, user_id)
            values (nextval('user_roles_id_seq'), ${now}, ${now}, ${userRole}, ${id})
        """
        sql.executeInsert(insertRole)
        user.toBuilder()
                .id(id)
                .build()
    }
}
