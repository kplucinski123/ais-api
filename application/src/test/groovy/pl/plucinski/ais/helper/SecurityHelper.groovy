package pl.plucinski.ais.helper

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.util.UriComponentsBuilder

@Component
class SecurityHelper {

    @Autowired TestRestTemplate restTemplate

    HttpHeaders securityHeader(String email, String password) {
        AuthenticateUserRequest authenticateUserRequest = AuthenticateUserRequest.newInstance(email, password)
        String url = UriComponentsBuilder.fromPath('/token').build()
        HttpEntity payload = new HttpEntity<>(authenticateUserRequest, new HttpHeaders())
        ResponseEntity<Token> responseEntity = restTemplate.exchange(url, HttpMethod.POST, payload, Token)
        HttpHeaders headers = new HttpHeaders()
        headers.set('token', responseEntity.body.token)
        headers
    }

    static class Token {
        String token
    }

    static class AuthenticateUserRequest {
        String email
        String password

        AuthenticateUserRequest(String email, String password) {
            this.email = email
            this.password = password
        }
    }
}
