package pl.plucinski.ais.helper

import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import pl.plucinski.ais.vessel.Vessel

import javax.sql.DataSource
import java.time.LocalDateTime

@Component
class VesselHelper {
    @Autowired DataSource dataSource

    Vessel vessel(String mmsi, String name) {
        Sql sql = new Sql(dataSource)
        LocalDateTime now = LocalDateTime.now()
        def shipType = 90
        def country = 'Poland'
        GString insertVessel = """
            insert into vessels (id, creation_date, modification_date, mmsi, ship_type, country, name)
            values (nextval('users_id_seq'), ${now}, ${now}, ${mmsi}, ${shipType}, ${country}, ${name})
        """
        GroovyRowResult[] results = sql.executeInsert(insertVessel, ['id'])
        Long id = (Long) results[0].get('id')
        Vessel.builder()
                .id(id)
                .name(name)
                .creationDate(now)
                .shipType(shipType)
                .country(country)
                .mmsi(mmsi)
                .build()
    }
}
