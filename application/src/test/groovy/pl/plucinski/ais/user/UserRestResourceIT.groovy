package pl.plucinski.ais.user

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.util.UriComponentsBuilder
import pl.plucinski.ais.CoreTest

class UserRestResourceIT extends CoreTest {
    @Autowired UserService userService

    def "should register user"() {
        given:
        def email = 'test123@test.pl'
        def userName = 'testUser'
        String url = UriComponentsBuilder.fromPath('/user')
                .build()
        CreateUserRequest request = CreateUserRequest.builder()
                .email(email)
                .userName(userName)
                .password('TestP@22word')
                .avatarId(UUID.randomUUID())
                .build()
        HttpEntity payload = new HttpEntity<>(request, HttpHeaders.EMPTY)

        when:
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, payload, Void)

        then:
        response.statusCode == HttpStatus.CREATED
        User user = userService.findByEmail(email)
        user
        user.email == email
        user.userName == userName
    }
}
