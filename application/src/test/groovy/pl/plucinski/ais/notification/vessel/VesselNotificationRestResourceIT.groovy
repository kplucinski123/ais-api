package pl.plucinski.ais.notification.vessel

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpEntity
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.util.UriComponentsBuilder
import pl.plucinski.ais.CoreTest
import pl.plucinski.ais.helper.VesselHelper
import pl.plucinski.ais.vessel.Vessel

class VesselNotificationRestResourceIT extends CoreTest {
    @Autowired VesselHelper vesselHelper

    def "should create vessel notification task"() {
        given:
        VesselNotificationRequest request = VesselNotificationRequest.builder()
                .deadLineInMinutes(360)
                .minLongitude(1.1)
                .minLatitude(1.1)
                .maxLongitude(1.1)
                .maxLatitude(1.1)
                .build()
        Vessel vessel = vesselHelper.vessel("1", "name")
        String url = UriComponentsBuilder.fromPath('/notifications/vessels/{vesselId}/tasks')
                .buildAndExpand(vessel.id)
        HttpEntity payload = new HttpEntity<>(request, userHeaders)

        when:
        ResponseEntity response = restTemplate.exchange(url, HttpMethod.POST, payload, Void)

        then:
        response.statusCode == HttpStatus.NO_CONTENT
    }
}
